#coding: utf-8

from bs4 import BeautifulSoup
import urllib.request
import re
import telegram
import logging

logging.basicConfig(level=logging.DEBUG, format='%(levelname) -8s %(message)s')

def cine(bot,update):
	infos=update.message.text.replace("/cine ","").split(" à ")
	if len(infos)==2:
		url = urllib.request.urlopen("http://www.google.fr/movies?near="+infos[1].replace(" ","+")+"&q="+infos[0].replace(" ","+").replace("+2D","").replace("+3D",""))
		page = str(url.read().decode('ISO-8859-1'))
		url.close()
		
		soup = BeautifulSoup(page,"html.parser")
		
		sug = [];
		for div in soup.find_all('div'):
			if div.get("class")==['movie']:
				sug.append(div.find_next(name="a").get("href"))
		
		if len(sug)==1:
			parse(bot,update,soup)
		elif len(sug)==0:
			bot.sendMessage(update.message.chat_id,"Pas de résultat :-/")
		elif "2D" in infos[0]:
			url = urllib.request.urlopen("http://www.google.fr"+sug[0])
			page = str(url.read().decode('ISO-8859-1'))
			url.close()
			soup = BeautifulSoup(page,"html.parser")
			parse(bot,update,soup)
		elif "3D" in infos[0]:
			url = urllib.request.urlopen("http://www.google.fr"+sug[1])
			page = str(url.read().decode('ISO-8859-1'))
			url.close()
			soup = BeautifulSoup(page,"html.parser")
			parse(bot,update,soup)
		else:
			ret=""
			for s in sug:
				ret+=s+"\n"
			bot.sendMessage(update.message.chat_id,"Veuillez préciser, 2D ou 3D ?")

def parse(bot,update,soup):
	for div in soup.find_all('div'):
		if div.get("class")==['theater']:
			retour=""
			for child in div.findChildren('div'):
				if child.get("class")==['name']:
					retour+=child.get_text()
				if child.get("class")==['times']:
					retour+="\n"+child.get_text()
			if "VF" in retour and "VO" in retour:
				retour=retour.replace("VO","\nVO")
			bot.sendMessage(update.message.chat_id,retour)

			
		
def main():
	token=input("Token Telegram : ")
	updater = telegram.Updater(token)
	
	dp = updater.dispatcher

	dp.addTelegramCommandHandler("cine", cine)	

	updater.start_polling()
	updater.idle()

if __name__ == '__main__':
	main()
